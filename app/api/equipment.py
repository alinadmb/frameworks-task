from flask import request, jsonify, url_for
from app.models import Equipment
from app.api import bp
from app import db
from app.api.errors import bad_request

@bp.route('/equipment/<int:id>', methods=['GET'])
def get_equip(id):
    return jsonify(Equipment.query.get_or_404(id).to_dict())

@bp.route('/equipment', methods=['GET'])
def get_all_equip():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10), 100)
    data = Equipment.to_collection_dict(Equipment.query, page, per_page, 'api.get_all_equip')
    return jsonify(data)

@bp.route('/equipment', methods=['POST'])
def create_equip():
    data = request.get_json() or {}
    if not data:
        return bad_request('Equipment should contain something')
    equip = Equipment()
    equip.from_dict(data)
    db.session.add(equip)
    db.session.commit()
    response = jsonify(equip.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_equip', id=equip.id)
    return response

@bp.route('/equipment/<int:id>', methods=['PUT'])
def update_equip(id):
    equip = Equipment.query.get_or_404(id)
    data = request.get_json() or {}
    if not data:
        return bad_request('Build should contain something')
    equip.from_dict(data)
    db.session.commit()
    return jsonify(equip.to_dict())

@bp.route('/equipment/<int:id>', methods=['DELETE'])
def remove_equip(id):
    equip = Equipment.query.get_or_404(id)
    db.session.delete(equip)
    db.session.commit()
    return jsonify(equip.to_dict())